package nauka.ćwiczenia;

class Wyniki {
    public static void main(String[] args) {
        Wyniki w = new Wyniki();
        w.doDziela();
    }

    void doDziela() {
        int wartosc = 7;
        for (int i = 1; i < 8; i++) {
            wartosc++;
            if (i > 4)  {
                System.out.print(++wartosc + " ");
            }
            if (wartosc > 14)   {
                System.out.println(" x = " + i);
                break;
            }
        }
    }
}
