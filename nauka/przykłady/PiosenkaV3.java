package nauka.przykłady;

import java.util.ArrayList;
import java.util.List;

public class PiosenkaV3 implements Comparable<PiosenkaV3> {
    private String tytul;
    private String artysta;
    private int bpm;

    public int compareTo(PiosenkaV3 s) {
        return tytul.compareTo(s.getTytul());
    }

    PiosenkaV3(String tytul, String artysta, int bpm) {
        this.tytul = tytul;
        this.artysta = artysta;
        this.bpm = bpm;
    }

    public String getTytul() {
        return tytul;
    }

    public String getArtysta() {
        return artysta;
    }

    public int getBpm() {
        return bpm;
    }

    public String toString() {
        return tytul;
    }

    static class AtrapaPiosenkiii {
        public static List<String> pobierzPiosenki() {
            List<String> piosenki = new ArrayList<>();
            piosenki.add("Autobiografia");
            piosenki.add("Brothers in arms");
            piosenki.add("Baśka");
            piosenki.add("Bombonierka");
            piosenki.add("Stairway to heaven");
            piosenki.add("Sultan of swing");
            piosenki.add("Numb");
            return piosenki;
        }

        public static List<PiosenkaV3> pobierzPiosenkiV3() {
            List<PiosenkaV3> piosenki = new ArrayList<>();
            piosenki.add(new PiosenkaV3("Autobiografia", "Perfect", 147));
            piosenki.add(new PiosenkaV3("Brothers in arms", "Dire Straits", 158));
            piosenki.add(new PiosenkaV3("Bombonierka", "Daniel", 170));
            piosenki.add(new PiosenkaV3("Baśka", "Wilki", 105));
            piosenki.add(new PiosenkaV3("Stairway to heaven", "Led Zeppelin", 158));
            return piosenki;
        }
    }
}
