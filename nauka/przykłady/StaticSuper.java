package nauka.przykłady;

class StaticSuper {
    static {
        System.out.println("Blok bazowego inicjalizatora statycznego.");
    }
    StaticSuper() {
        System.out.println("Konstruktor bazowy");
    }
}
