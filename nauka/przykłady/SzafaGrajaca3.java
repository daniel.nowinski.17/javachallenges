package nauka.przykłady;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SzafaGrajaca3 {
    public static void main(String[] args) {
        new SzafaGrajaca3().doDziela();
    }
    public void doDziela() {
        List<String> listaPiosenek = AtrapaPiosenkiii.pobierzPiosenkiV3();
        System.out.println(listaPiosenek);
        Collections.sort(listaPiosenek);
        System.out.println(listaPiosenek);
    }
}
class AtrapaPiosenkiii {
    public static List<String> pobierzPiosenkiV3()    {
        List<String> piosenki = new ArrayList<>();
        piosenki.add("Autobiografia");
        piosenki.add("Brothers in arms");
        piosenki.add("Baśka");
        piosenki.add("Bombonierka");
        piosenki.add("Stairway to heaven");
        piosenki.add("Sultan of swing");
        piosenki.add("Numb");
        return piosenki;
    }
}
