package nauka.przykłady;

class Piesek {
    int wielkosc;
    String imie;
    void szczekaj() {
        if (wielkosc > 23) {
            System.out.println("Houuu, houu");
        } else if (wielkosc > 6) {
            System.out.println("Chau, Chau");

        } else {
            System.out.println("Hiau, Hiau");
        }
    }
}
class PiesekTester {
    public static void main(String[] args) {
        Piesek pierwszy = new Piesek();
        pierwszy.wielkosc = 40;
        Piesek drugi = new Piesek();
        drugi.wielkosc = 2;
        Piesek trzeci = new Piesek();

        trzeci.wielkosc = 8;

        pierwszy.szczekaj();
        drugi.szczekaj();
        trzeci.szczekaj();
    }
}


