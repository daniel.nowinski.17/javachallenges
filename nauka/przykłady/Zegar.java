package nauka.przykłady;

public class Zegar {
    String czas; // prywatne pole "czas", przechowujące aktualny czas

    void setCzas(String c) { // publiczna metoda umożliwiająca ustawienie czasu
        czas = c; // przypisanie wartości parametru "c" do pola "czas"
    }

    String getCzas() { // publiczna metoda zwracająca aktualny czas
        return czas; // zwrócenie wartości pola "czas"
    }
}

class ZegarTester {
    public static void main(String[] args) {
        Zegar z = new Zegar(); // utworzenie nowego obiektu klasy Zegar

        z.setCzas("123456"); // ustawienie czasu za pomocą metody setCzas
        String dta = z.getCzas(); // pobranie aktualnego czasu za pomocą metody getCzas i przypisanie go do zmiennej "dta"
        System.out.println("Czas: " + dta ); // wyświetlenie aktualnego czasu
    }
}
