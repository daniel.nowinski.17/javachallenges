package nauka.przykłady;

class rozdz3A {
    String tytul;
    String autor;
}
class rozdz3ATester {
    public static void main(String[] args) {


        rozdz3A[] mojeKsiazki = new rozdz3A[3];
        int x = 0;
        mojeKsiazki[0] = new rozdz3A();
        mojeKsiazki[1] = new rozdz3A();
        mojeKsiazki[2] = new rozdz3A();
        mojeKsiazki[0].tytul = "Czterej koderzy i Java";
        mojeKsiazki[1].tytul = "Java nocy letniej";
        mojeKsiazki[2].tytul = "Java. Receptury";
        mojeKsiazki[0].autor = "janek";
        mojeKsiazki[1].autor = "wilhelm";
        mojeKsiazki[2].autor = "ian";
        while (x < 3) {
            System.out.print(mojeKsiazki[x].tytul);
            System.out.print(", autor ");
            System.out.println(mojeKsiazki[x].autor);
            x = x + 1;
        }
    }
}