package nauka.przykłady;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SzafaGrajaca1  {
    public static void main(String[] args) {
        new SzafaGrajaca1().doDziela();
    }
    public void doDziela() {
        List<String> listaPiosenek = AtrapaPiosenki.pobierzPiosenki();
        System.out.println(listaPiosenek);
        Collections.sort(listaPiosenek);
        System.out.println(listaPiosenek);
    }
}

class AtrapaPiosenki {
    public static List<String> pobierzPiosenki()    {
        List<String> piosenki = new ArrayList<>();
        piosenki.add("Autobiografia");
        piosenki.add("Brothers in arms");
        piosenki.add("Baśka");
        piosenki.add("Bombonierka");
        piosenki.add("Stairway to heaven");
        piosenki.add("Sultan of swing");
        piosenki.add("Numb");
        return piosenki;
    }
}
