package nauka.zadania.myAdventure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Room {

    private String description;         //opis pokoju
    private List<String> items;          //lista przedmiotów
    private Map<String, Room> exist;    //wyjścia z pokoju

    public Room(String description) {
        this.description = description;
        this.items = new ArrayList<>();     //inicjalizacja pustej listy itemów
        this.exist = new HashMap<>();       //inicjalizacja pustej mapy wyjść

    }

    //zwraca opis pokoju
    public String getDescription() {
        return description;
    }

    public void addItem(String item) {
        items.add(item);

    }

    public boolean removeItem(String item) {       //usuwa item z pokoju
        return items.remove(item);
    }

    public List<String> getItems() {               //zwraca listę itemów
        return items;
    }

    public void setExit(String direction, Room room) {         //ustawia wyjście w określonym kierunku
        exist.put(direction.toLowerCase(), room);
    }

    public Room getExit(String direction) {
        return exist.get(direction.toLowerCase());              //Zwraca pokój w określonym kierounku
    }

    public String getExits() {
        return String.join(", ", exist.keySet());
    }
}


