package nauka.zadania.myAdventure;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        Room entryRoom = new Room("Jesteś w jasnym pokoju z zamkniętą skrzynią ;)");            //Tworzymy pokoje
        Room darkRoom = new Room("To skrytka w której nie ma źródła światła. Nic nie widzisz.");

        entryRoom.addItem("klucz");         //dodajemy przedmioty do pokoi
        darkRoom.addItem("latarka");

        entryRoom.setExit("południe", darkRoom);        //wyjścia między pokojami
        darkRoom.setExit("północ", entryRoom);

        Room currentRoom = entryRoom;       //przypisanie aktualnego pokoju

        List<String> investory = new ArrayList<>();
        //Przywitanie gracza
        System.out.println("Witam w mojej grze <high-five>");
        System.out.println("Wpisz 'pomoc' by zobaczyć listę dostępnych komend");

        //Pętla gry
        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        while (running) {
            System.out.println("> ");   //wskaźnik, że gracz ma coś wpisać
            String command = scanner.nextLine();

            if (command.equalsIgnoreCase("pomoc")) {
                System.out.println("Dostępne komendy:");
                System.out.println("  - pomoc: wyświetla listę komend");
                System.out.println("  - opisz: opisuje aktualne pomieszczenie");
                System.out.println("  - idź KIERUNEK: przejście do innego pomieszczenia");
                System.out.println("  - zabierz PRZEDMIOT: zabranie przedmiotu z pokoju");
                System.out.println("  - przedmioty: pokazuje ekwipunek gracza");
                System.out.println("  - użyj PRZEDMIOT: użycie przedmiotu");
                System.out.println("  - koniec: kończy grę");
            } else if (command.equalsIgnoreCase("opisz")) {
                System.out.println("To tylko wstępna wersja gry, tutaj będzie opis pokoju.");
            } else if (command.startsWith("idź")) { // Sprawdza, czy komenda zaczyna się od "idź "
                System.out.println("Próbujesz przejść w kierunku: " + command.substring(3));
                System.out.println("Funkcja 'idź' będzie rozwinięta w kolejnych krokach.");
            } else if (command.startsWith("zabierz")) { // Sprawdza, czy zaczyna się od "zabierz "
                System.out.println("Próbujesz zabrać: " + command.substring(7));
                System.out.println("Komenda 'zabierz' będzie dopracowana w dalszych krokach");
            } else if (command.startsWith("użyj")) { // Sprawdza, czy zaczyna się od "użyj "
                System.out.println("Próbujesz użyć " + command.substring(4));
                System.out.println("Komenda 'użyj' będzie rozbudowana w dalszych krokach");
            } else if (command.equalsIgnoreCase("koniec")) {
                System.out.println("Dziękuję za grę. Do zobaczenia!");
                running = false;
            } else {
                System.out.println("Nieznana komenda. Wpisz 'pomoc', by wyświetlić dostępne komendy.");
            }

        }
        scanner.close(); //Zamykamy scannera po zakończeniu gry
    }
}
