package nauka.zadania;

import java.util.List;
import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(63, 45, 95, 74, 23, 11, 84, 6, 1);// Utworzenie nowej Arr przechowującej liczby.

        bubbleSort(numbers); // wywołanie metody sortującej

        System.out.println("Posortowana lista: " + numbers);
    }

    public static void bubbleSort(List<Integer> arr) { //Metoda sortująca, argument ArrayList z liczbami.
        int n = arr.size(); //Przypisanie do n rozmiary listy.
        for (int i = 0; i < n - 1; i++) {  //pętla for przebiegająca przez wszystkie elementy listy
            for (int j = 0; j < n - i - 1; j++) {  //wewnętrzna pętla for przebiegająca od początku do 'n - i - 1'
                if (arr.get(j) > arr.get(j + 1)) {  //porównanie sąsiednich elementów listy
                    int temp = arr.get(j); // Przechowanie bieżącego elementu w zmiennej tymczasowej.
                    arr.set(j, arr.get(j + 1)); // przypisanie nastepnego elementu na obecne miejsce,
                    arr.set(j + 1, temp); // Przypisanie wartości z tymczasowej na miejsce następnego elementu.
                }
            }
        }

    }
}
